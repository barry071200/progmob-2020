package com.example.progmob_2020.CrudMatKul;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.CrudDosen.DosenAddActivity;
import com.example.progmob_2020.CrudDosen.DosenGetAllActivity;
import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);

        final EditText txtKodeMatkul = findViewById(R.id.txtKodeMatkul);
        final EditText txtNamaMatkul = findViewById(R.id.txtNamaMatkul);
        final EditText txtHari = findViewById(R.id.txtHari);
        final EditText txtSesi = findViewById(R.id.txtSesi);
        final EditText txtSks = findViewById(R.id.txtSks);
        Button btnSimpanMatkul = findViewById(R.id.btnSimpanMatkul);
        pd = new ProgressDialog(MatkulAddActivity.this);

        btnSimpanMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Sek Sebentar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        txtKodeMatkul.getText().toString(),
                        txtNamaMatkul.getText().toString(),
                        txtHari.getText().toString(),
                        txtSesi.getText().toString(),
                        txtSks.getText().toString(),
                        "72170169"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "Berhasil Disimpan!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MatkulAddActivity.this, MatkulGetAllActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "GAGAL!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
