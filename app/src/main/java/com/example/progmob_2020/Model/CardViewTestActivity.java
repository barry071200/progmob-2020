package com.example.progmob_2020.Model;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
//import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_2020.Adapter.MahasiswaRecycleAdapter;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

        RecyclerView cv = findViewById(R.id.rvCard);
        MahasiswaRecycleAdapter mahasiswaCardAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("salvador", "721708183", "08946245184");
        Mahasiswa m2 = new Mahasiswa("tatan", "72170662", "08956245155");
        Mahasiswa m3 = new Mahasiswa("kur", "72170416", "08966245196");
        Mahasiswa m4 = new Mahasiswa("riki", "72170260", "08976245127");
        Mahasiswa m5 = new Mahasiswa("edo", "72170154", "08986245108");
        Mahasiswa m6 = new Mahasiswa("mono", "72160258", "08926245129");
        Mahasiswa m7 = new Mahasiswa("lisa", "72170126", "08916245130");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);
        mahasiswaList.add(m6);
        mahasiswaList.add(m7);

        mahasiswaCardAdapter= new MahasiswaRecycleAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        cv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        cv.setAdapter(mahasiswaCardAdapter);
    }
}