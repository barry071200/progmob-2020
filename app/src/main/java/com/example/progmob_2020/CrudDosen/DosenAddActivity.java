package com.example.progmob_2020.CrudDosen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText txtNama = findViewById(R.id.txtNama);
        final EditText txtNidn = findViewById(R.id.txtNidn);
        final EditText txtAlamat = findViewById(R.id.txtAlamat);
        final EditText txtEmail = findViewById(R.id.txtEmail);
        final EditText txtGelar = findViewById(R.id.txtGelar);
        Button btnSimpan = findViewById(R.id.btnSimpan);
        pd = new ProgressDialog(DosenAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Sek Sebentar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        txtNama.getText().toString(),
                        txtNidn.getText().toString(),
                        txtAlamat.getText().toString(),
                        txtEmail.getText().toString(),
                        txtGelar.getText().toString(),
                        "Kosongkan aja",
                        "72170169"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "Berhasil Disimpan!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DosenAddActivity.this, DosenGetAllActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "GAGAL!", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }
}
